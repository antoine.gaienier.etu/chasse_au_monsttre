Antoine Gaienier :

    - semaine 45 (du 6/11 au 12/11) : coorection de la génération du lab / ajout de test a Mazes /
    - semaine 46 (du 13/11 au 19/11) : Mise en place de régle de fin de partie
    - semaine 47 (du 20/11 au 26/11) : 
    - semaine 48 (du 27/11 au 3/12) : Malade
    - semaine 49 (du 4/12 au 10/12) : refacto de maze / choisir le % de Wall / plusieur chemin dans le maze/ correction bug (sortie pastoujours accessible) / Boost pour le Montre et le Hunter
    - semaine 50 (du 11/12 au 17/12) :
    - semaine 51 (du 18/12 au 24/12) : JavaDoc de main.map et main.player
    - semaine 52 (du 25/12 au 31/12) :
    - semaine 01 (du 01/01 au 07/01) : tentative de correction de bug sans succès
    - semaine 02 (du 08/01 au 14/01) : Refonte de player (hunter)

Quentin Moutte :

    - semaine 45 (du 6/11 au 12/11) : refactorisation du code javafx.
    - semaine 46 (du 13/11 au 19/11) : ajout de fenetre (parametre et choix joueur), Monstre voit le tir du chasseur, et ajout d'un temps de latence pour que le joueur puisse voir son action.
    - semaine 47 (du 20/11 au 26/11) : modification du code pour utiliser design pattern MVC.
    - semaine 48 (du 27/11 au 3/12) : Correction d'un bug qui empecher la victoire, debut d'ajout d'une classe Controller.java et de fichiers fxml afin de simplifier la gestion des fenêtres.
    - semaine 49 (du 4/12 au 10/12) : interactivité du fxml, refactorisation, implementation des ia dans le javafx
    - semaine 50 (du 11/12 au 17/12) : refactorisation, réorganisation des classe affichage ia vs ia réparer
    - semaine 51 (du 18/12 au 24/12) : ajout fenetre pour labyrinthe, limitation taille du labyrinthe à l'écran, touche pour ia contre ia
    - semaine 52 (du 25/12 au 31/12) : transformation MVC
    - semaine 01 (du 01/01 au 07/01) :
    - semaine 02 (du 08/01 au 14/01) : création strategy dans vue (pour modifier comportement selon joueur et IA), raccorder la vue avec les strategie Monster et Hunter, javadoc, restructuration des dossiers et fichiers.

Victor Singez :

    - semaine 45 (du 6/11 au 12/11) : Ajout des méthodes permettant au Hunter de gagner lorsqu'il a trouvé le monstre.
    - semaine 46 (du 13/11 au 19/11) :
    - semaine 47 (du 20/11 au 26/11) : refactorisation de la méthode shoot de la classe Hunter.java pour obtenir un code plus propre.
    - semaine 48 (du 27/11 au 3/12) : 
    - semaine 49 (du 4/12 au 10/12) : Amélioration de l'affichage des menus du jeu avec modification et amélioration des fichiers Menu.fxml, Parametre.fxml et ChoixJoueur.fxml et ajout affichage si victoire chasseur.
    - semaine 50 (du 11/12 au 17/12) : Modification fichiers fxml pour amélioration des différents menus.
    - semaine 51 (du 18/12 au 24/12) : Modification et amélioration des différents menus du jeu.
    - semaine 52 (du 25/12 au 31/12) :
    - semaine 01 (du 01/01 au 07/01) :
    - semaine 02 (du 08/01 au 14/01) : Amélioration et résolution des problèmes/bugs des menus du jeu.

Raphael Pladys :

    - semaine 45 (du 6/11 au 12/11) : Test et Refactor de Monster
    - semaine 46 (du 13/11 au 19/11) : Aide groupe pour les Mazes et le Hunter
    - semaine 47 (du 20/11 au 26/11) : Début IA Monster
    - semaine 48 (du 27/11 au 3/12) : IA Monster finie, début IA Hunter
    - semaine 49 (du 4/12 au 10/12) : IA Hunter finie
    - semaine 50 (du 11/12 au 17/12) : refactorisation et réorganisation des classes
    - semaine 51 (du 18/12 au 24/12) : réadaptation à l'énoncé et refactor pour les IA
    - semaine 02 (du 08/01 au 14/01) : correction des IA et amélioration de l'IA monster



    TODO: 
    Raph : doc et bug (s'il y en a)
    Quentin: correction affichage - barre de nav - touche pour ia contre ia
    Antoine : faire des putin de maze rectangle.
    
    
    commande javadoc :  javadoc -d ../doc/ --module-path ../lib/javafx --add-modules=javafx.controls,javafx.fxml main.Vue main.map main.Modele main.player

