# SAE 3.02 Dev.applications ( Chasse au monstre )

## Auteurs
- Antoine Gaienier
- Quentin Moutte
- Victor Singez
- Raphael Pladys

## Ressources Impliquées

- R3.02 Développement efficace
- R3.03 Analyse
- R3.04 Qualité de développement

## Description

Bienvenue sur le projet Chasse au Monstre. C'est un jeu dans lequel 2 joueur s'affronte, un Chasseur et un Monstre. Le Monstre se trouve dans un labyrinthe et doit atteindre la sortie de celui-ci pour gagner. Le Chasseur doit trouver le monstre alors qu'il ne connait pas le labyrinthe en "tirant" sur les cases pour les découvrir, il gagne si il tire sur le Monstre.
Le jeu se déroule au tour par tour, d'abord le monstre joue, et ensuite, le chasseur.
Le jeu peut se jouer à 2, ou seul en utilisant un ia.
On peut aussi faire jouer les IAs les une contres les autres, juste en regardant la partie se dérouler.

Il a été programmé en java, et utilise javafx pour l'interface graphique du jeu, avec des fichiers fxml pour les fenêtres.

## Lancement de l'application
Pour pourvoir lancer l'application, vous devez ouvrir un terminal, vous placer à la racine du dossier, puis de taper :
 
java --module-path lib/javafx/ --add-modules ALL-MODULE-PATH -jar "H4_SAE3A.jar"