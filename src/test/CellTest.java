package test;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

import fr.univlille.iutinfo.cam.player.perception.ICellEvent.CellInfo;
import main.Modele.map.Cell;
import main.Modele.map.Coordinate;

public class CellTest {
    // test la methode getState
    @Test
    public void test_getState(){
        Cell cell = new Cell(new Coordinate(0, 0));
        assertTrue(cell.getState() == CellInfo.EMPTY);
    }
    // test si a la création d'une cellule, celle-ci est bien vide
    @Test
    public void test_empty_cell(){
        Cell cell = new Cell(new Coordinate(0, 0));
        assertTrue(cell.getState() == CellInfo.EMPTY);
    }
    // test si la cellule n'est plus vide après avoir utiliser la méthode setInfo
    @Test
    public void test_setInfo(){
        Cell cell = new Cell(new Coordinate(0, 0));
        cell.setInfo(CellInfo.WALL);
        assertTrue(cell.getState() != CellInfo.EMPTY);
    }    
}
