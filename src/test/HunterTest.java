package test;

import fr.univlille.iutinfo.cam.player.perception.ICellEvent.CellInfo;
import main.Modele.map.Coordinate;
import main.Modele.map.Mazes;
import main.Modele.player.playerRework.Hunter;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class HunterTest {

    private static Mazes mazes;
    private static Hunter hunter;

    @Test
    public void testHunter() {
        mazes = new Mazes(5, 5, (CellInfo) null);
        hunter = new Hunter(mazes);
        assertTrue(hunter.shoot(new Coordinate(1, 4)));
        assertTrue(hunter.shoot(new Coordinate(10, 3)));
    }

}