package test;


import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import main.Modele.map.Coordinate;
import main.Modele.player.playerRework.Monster;

public class MonsterTest {
    Monster mon;

    @BeforeEach
    public void init() {
        mon = new Monster(new Coordinate(1, 1), new boolean[][]{{true,true,false},{true,false,false},{false,false,false}});
        // mon.initialize(new boolean[][]{{true,true,false},{true,false,false},{false,false,false}});
    }

    //Teste ce que la fonction isValidMove renvoie lorsque le maze contient un mur sur la case sélectionnée ou non
    @Test
    public void isValidMoveWallTest() {
        assertFalse(mon.isValidMove(new Coordinate(1, 0)));
        assertFalse(mon.isValidMove(new Coordinate(0, 1)));
        assertTrue(mon.isValidMove(new Coordinate(1, 2))); //Le monstre s'est déplacé en (1,2)
        mon.setCoord(1, 1);
        assertTrue(mon.isValidMove(new Coordinate(2, 1)));
    }

    //Teste ce que la fonction isValidMove renvoie lorsque la case sélectionnée est à côté ou non
    @Test
    public void isValidMoveDisplacementTest() {
        assertTrue(mon.isValidMove(new Coordinate(2, 1)));
        assertTrue(mon.isValidMove(new Coordinate(1, 1)));
        mon.isValidMove(new Coordinate(0, 1));
        assertEquals(mon.getCoord(), new Coordinate(1, 1));
    }

    //Teste que la fonction isValidMove ne permet pas au monstre de sortir des limites du labyrinthe
    @Test
    public void isValidMoveOOBTest() {
        mon.setCoord(2, 2);
        assertFalse(mon.isValidMove(new Coordinate(3, 2)));
        assertFalse(mon.isValidMove(new Coordinate(2, 3)));
        mon.setCoord(0, 0);
        assertFalse(mon.isValidMove(new Coordinate(-1, 0)));
        assertFalse(mon.isValidMove(new Coordinate(0, -1)));
    }
}
