package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import fr.univlille.iutinfo.cam.player.perception.ICellEvent.CellInfo;
import main.Modele.map.*;


public class MazesTest {    
    // test si il y a une entrée 
    @Test
    public void test_entry(){
        int entry = 0;
        
        for(int i = 0; i < 100; i++){
            Mazes mazes = new Mazes(5, 5, 0.0,0.0);
            for(int j = 0; j < mazes.getWidth(); j++){
                for(int k = 0; k < mazes.getHeight(); k++){
                    if(mazes.getCell(j, k).getState() == CellInfo.MONSTER){
                        entry++;
                    }
                }
            }
        }
        assertEquals(entry , 100);
    }

    // test si il y a une sortie
    @Test
    public void test_exit(){
        int exit = 0;
        for(int i = 0; i < 100; i++){
            Mazes mazes = new Mazes(5, 5,0.0,0.0);
            for(int j = 0; j < mazes.getWidth(); j++){
                for(int k = 0; k < mazes.getHeight(); k++){
                    if(mazes.getCell(j, k).getState() == CellInfo.EXIT){
                        exit++;
                    }
                }
            }
        }
        assertEquals(exit , 100);
    }

    // test si les sortie sont bien au bord du labyrinthe
    @Test
    public void test_exit_border(){
        int onBorderer = 0;
        for(int i = 0; i < 100; i++){
            Mazes mazes = new Mazes(5, 5,0.0,0.0);
            if(mazes.getExit().getRow() == 0 || mazes.getExit().getRow() == mazes.getWidth()-1 || mazes.getExit().getCol() == 0 ||  mazes.getExit().getCol() == mazes.getHeight()-1){
                onBorderer ++;
            }
        }
        assertEquals(100, onBorderer);
    }
    
    // test si l'entée est bien au bord du labyrinthe + 1
    @Test
    public void test_entry_border(){
        int onBorderer = 0;
        for(int i = 0; i < 100; i++){
            Mazes mazes = new Mazes(5, 5,0.0,0.0);
            if(mazes.getEntry().getRow() == 1 || mazes.getEntry().getRow() == mazes.getWidth()-2 || mazes.getEntry().getCol() == 1 ||  mazes.getEntry().getCol() == mazes.getHeight()-2){
                onBorderer ++;
            }
        }
        assertEquals(100, onBorderer);
    }

    //test removeWall
    @Test
    public void test_removeWall(){
        Mazes mazes= new Mazes(10,10 , 1.0,0.0);
        int size = mazes.getListWall().size(); 
        mazes.setObstaclePercentage(0.5);
        mazes.removeWall();
        assertTrue(size > mazes.getListWall().size());

    }
}
