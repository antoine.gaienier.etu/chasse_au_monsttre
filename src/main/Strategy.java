package main;

import main.Modele.map.Coordinate;

public interface Strategy {
        public void update(Coordinate coord);
}