package main;

import javafx.application.Application;
import javafx.stage.Stage;
import main.Modele.Game;

public class Launch extends Application {

    private static Game game;

    @Override
    public void start(Stage arg0) throws Exception {
        game = new Game();
        game.start();
    }
 
    public static void main(String[] args) {
        Application.launch(args);
    }

    /**
     * Permet de renvoyer l'attribut game
     * @return l'attribut game
     */
    public static Game getGame() {
        return game;
    }
}
 