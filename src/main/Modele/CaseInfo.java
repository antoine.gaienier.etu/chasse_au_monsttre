package main.Modele;

import javafx.scene.paint.Color;
import fr.univlille.iutinfo.cam.player.perception.ICellEvent.CellInfo;

public enum CaseInfo {
    WALL(CellInfo.WALL, Color.GREY, null),
    MONSTER(CellInfo.MONSTER, Color.LIGHTBLUE, "M"),
    HUNTER(CellInfo.HUNTER, Color.RED, "H"),
    EMPTY(CellInfo.EMPTY, Color.LIGHTBLUE, null),
    EXIT(CellInfo.EXIT, Color.LIGHTBLUE, "E");

    private CellInfo cellInfo;
    private Color color;
    private String title;
    
    CaseInfo(CellInfo cellInfo, Color color, String title) {
        this.cellInfo = cellInfo;
        this.color = color;
        this.title = title;
    }

    public CellInfo getCellInfo() {
        return cellInfo;
    }

    public Color getColor() {
        return color;
    }

    public String getTitle() {
        return title;
    }

    public void setColor(Color color) {
        this.color = color;
    }

}
