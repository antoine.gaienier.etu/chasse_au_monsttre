package main.Modele.map;

import fr.univlille.iutinfo.cam.player.perception.ICoordinate;

/**
 * Classe qui représente une coordonnée
 */
public class Coordinate implements ICoordinate {
    private int row;
    private int col;

    /**
     * Constructor de la clase Coordinate
     * @param row (l'indice de la ligne)
     * @param col (l'indice de la colonne)
     */
    public Coordinate(int row, int col) {
        this.row = row;
        this.col = col;
    }
    
    /**
     * Retourne la ligne de la coordonnée
     * @return
     */
    public int getRow() {
        return row;
    }

    /**
     * Retourne la colonne de la coordonnée
     * @return
     */
    public int getCol() {
        return col;
    }

    public String toString() {
        return getCol() + ";" + getRow();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Coordinate other = (Coordinate) obj;
        if (row != other.row)
            return false;
        if (col != other.col)
            return false;
        return true;
    }

    
}
