package main.Modele.map;

/**
 * Enumération des différents voisins
 */
public enum Voisin {
    DROITE(0, 1),
    BAS(1, 0),
    GAUCHE(0, -1),
    HAUT(-1, 0);

    private int deltaX, deltaY;

    /**
     * Retourne le déplacement en X
     * @return
     */
    public int getDeltaX() {
        return deltaX;
    }

    /**
     * Retourne le déplacement en Y
     * @return
     */
    public int getDeltaY() {
        return deltaY;
    }

    /**
     * Constructeur de la classe Voisin
     * @param x
     * @param y
     */
    private Voisin(int x, int y) {
        deltaX = x;
        deltaY = y;
    }
}