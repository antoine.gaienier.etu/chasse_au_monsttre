package main.Modele.map;
 
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import fr.univlille.iutinfo.cam.player.perception.ICellEvent.CellInfo;
import main.java.fr.univlille.iutinfo.utils.Subject;


/**
 * Classe qui représente un labyrinthe
 */
public class Mazes extends Subject{
    private int width;
    private int height;
    private Cell[][] maze;
    protected List<Coordinate> listWall;
    private List<Coordinate> ListEmptyCell;
    private Random random;
    private Coordinate entry;
    private Coordinate exit;
    private Double obstaclePercentage;
    private Double boostPercentage;

    /**
     * Constructeur de la classe Mazes.
     * @param width
     * @param height
     * @param obstaclePercentage
     * @param boostPercentage
     */
    public Mazes(int width, int height, Double obstaclePercentage, Double boostPercentage) {
        if (width % 2 == 0) this.width = width+1;
        else this.width = width;
        if (height % 2 == 0) this.height = height+1;
        else this.height = height;
        if (this.width < 3) this.width = 3;
        if (this.height < 3) this.height = 3;
        this.listWall = new ArrayList<Coordinate>(width*height);
        this.ListEmptyCell = new ArrayList<Coordinate>(width*height);
        this.maze = new Cell[this.height][this.width];
        this.random = new Random();
        this.obstaclePercentage = obstaclePercentage;
        this.boostPercentage = boostPercentage;
        initializeMaze();
        generateMaze(1 ,1);
        setDistantEntryAndExit();
        removeWall();
        setBoosts();

        
    }

    /**
     * Constructeur de la classe Mazes.
     * @param width
     * @param height
     * @param cellInfo
     */
    public Mazes(int width, int height, CellInfo cellInfo) {
        if (width % 2 == 0) this.width = width+1;
        else this.width = width;
        if (height % 2 == 0) this.height = height+1;
        else this.height = height;
        if (this.width < 3) this.width = 3;
        if (this.height < 3) this.height = 3;

        this.maze = new Cell[this.height][this.width];
        for (int i = 0; i < this.height ; i++) {
            for (int j = 0; j < this.width ; j++) {
                maze[i][j] = new Cell(new Coordinate(i, j));
                maze[i][j].setInfo(cellInfo);
            }
        }
    }

    /**
     * initialise le maze avec des murs 
     */
    private void initializeMaze() {
        for (int i = 0; i < this.height ; i++) {
            for (int j = 0; j < this.width ; j++) {
                maze[i][j] = new Cell(new Coordinate(i, j));
                maze[i][j].setInfo(CellInfo.WALL);
                if(validCoord(i, j)){
                    listWall.add(new Coordinate(i, j));
                }
                
            }
        }
    }

    /**
     * Génère le labyrinthe en utilisant l'algorithme de backtracking de manière récursive
     * @param x correspond à la position x de la cellule
     * @param y correspond à la position y de la cellule 
     */
    private void generateMaze(int x, int y) {
        maze[y][x].setInfo(CellInfo.EMPTY);
        int[][] directions = {{0, 2}, {2, 0}, {0, -2}, {-2, 0}};
        shuffleArray(directions);
        for (int[] direction : directions) {
            int dx = direction[1];
            int dy = direction[0];
            int nextX = x + dx;
            int nextY = y + dy;
            if (nextX > 0 && nextX < width && nextY > 0 && nextY < height && maze[nextY][nextX].getState()==CellInfo.WALL) {
                int wallX = x + dx / 2;
                int wallY = y + dy / 2;
                maze[wallY][wallX].setInfo(CellInfo.EMPTY);
                listWall.remove(new Coordinate(wallY, wallX));
                ListEmptyCell.add(new Coordinate(wallY, wallX));
                generateMaze(nextX, nextY);
            }
        }
    }

    /**
     * Place l'entrée et la sortie du labyrinthe à des positions aléatoires.
     */
    private void setDistantEntryAndExit() {
        int sideEntry;
        int xExit=0;
        int yExit = 0;
        int xEntry = 0;
        int yEntry =0;
        boolean out = false;
        do {
            sideEntry = random.nextInt(4); // côté pour l'entrée aléatoire
            switch (sideEntry) {
                case 0: // Haut
                    xEntry = 1;
                    yEntry = random.nextInt(width-2)+1;
                    xExit = height-1;
                    yExit = random.nextInt(width-2)+1;
                    break;
                case 1: // Droite
                    xEntry = random.nextInt(height-2)+1;
                    yEntry = width-2;
                    xExit = random.nextInt(height-2)+1;
                    yExit = 0;
                    break;
                case 2: // Bas
                    xEntry = height-2;
                    yEntry = random.nextInt(width-2)+1;
                    xExit = 0;
                    yExit = random.nextInt(width-2)+1;
                    break;
                case 3: // Gauche
                    xEntry = random.nextInt(height-2)+1;
                    yEntry = 1;
                    xExit = random.nextInt(height-2)+1;
                    yExit = width-1;
                    break;
            }
            if (isAccessible(yExit, xExit)){
                out = true;
            }
        }
        while ( !out);
        maze[xEntry][yEntry].setInfo(CellInfo.MONSTER); 
        maze[xExit][yExit].setInfo(CellInfo.EXIT);
        listWall.remove(new Coordinate(xEntry, yEntry));
        setEntry(new Coordinate(xEntry, yEntry));
        setExit(new Coordinate(xExit, yExit));
    }

    /**
     * regarde si la cell est accessible ou non, c a dire un des 4 voisins est vide
     * @param x
     * @param y
     * @return true si la cell est accessible
     */
    public boolean isAccessible(int x, int y) {
        for (Voisin v : Voisin.values()) {
            int xVoisin = x + v.getDeltaX();
            int yVoisin = y + v.getDeltaY();
            try{
                if (maze[yVoisin][xVoisin].getState() == CellInfo.EMPTY) {
                    return true;
                }
            }catch(ArrayIndexOutOfBoundsException e){}
        }
        return false;
    }

    /** melange les direction de génération du labyrinthe
     * @param array tableau de direction
     */
    private void shuffleArray(int[][] array) {
        int index , tempX , tempY;
        for (int i = array.length - 1; i > 0; i--) {
            index = random.nextInt(i + 1);
            tempX = array[index][0];
            tempY = array[index][1];
            array[index][0] = array[i][0];
            array[index][1] = array[i][1];
            array[i][0] = tempX;
            array[i][1] = tempY;
        }
    }

    /**
     * supprime X% des murs du labyrinthe restant après génération du labyrinthe
     */
    public void removeWall() {
        int nbWall = (int) (listWall.size()*(1- obstaclePercentage));
        for (int i = 0; i < nbWall; i++) {
            int index = random.nextInt(listWall.size());
            Coordinate coord = listWall.get(index);
            removeOneWall(coord);
        }
    }

    /**
     * supprime un mur du labyrinthe et le remplace par une case vide 
     * @param wall
     */
    public void removeOneWall( Coordinate wall){
        if(!validCoord(wall)){
            throw new IllegalArgumentException("Coordonnées non valides");
        } 
        else{ 
            maze[wall.getRow()][wall.getCol()].setInfo(CellInfo.EMPTY);
            listWall.remove(wall);
            ListEmptyCell.add(wall);
        }
    }

    /**
     * place des Boost dans le labyrinthe de manière aléatoire sur des cases vides
     */
    public void setBoosts(){
        int nbEvent = (int) (ListEmptyCell.size()*(boostPercentage));
        for (int i = 0; i < nbEvent; i++) {
            int index = random.nextInt(ListEmptyCell.size());
            Coordinate coord = ListEmptyCell.get(index);
            
            // chaoisir un Event aléatoire
            int event = random.nextInt(Boost.values().length);
            int j = 0;
            for (Boost e : Boost.values()) {
                if( j == event){
                    maze[coord.getRow()][coord.getCol()].setBoost(e);
                    break;
                }
                j++;
            }
        }

    }

    /**
     * retourne true si les coordonnées sont valides (dans le cadre labyrinthe) 
     * @param coord
     * @return true si les coordonnées sont valides
     */
    public boolean validCoord(Coordinate coord){
        return coord.getRow() > 0 && coord.getCol() > 0 &&  coord.getCol() < width-1 &&  coord.getRow() < height-1;
    }

    /**
     * retourne true si les coordonnées sont valides (dans le cadre labyrinthe)
     * @param x
     * @param y
     * @return
     */
    public boolean validCoord(int x, int y){
        return validCoord(new Coordinate(x, y));
    }

    /**
     * retourne la position du monstre
     * @return la position du monstre
     */
    public Coordinate MonsterPose(){
        Coordinate res = new Coordinate(0, 0);
        for (int i = 0; i < height ; i++) {
            for (int j = 0; j < width ; j++) {
                Cell c = maze[i][j];
                if (c.getState() == CellInfo.MONSTER) {
                    return res = c.getCoord();
                }
            }
        }          
        return res;
    }

    /**
     * retourne la cellule aux coordonnées données
     * @param width de la cellule
     * @param height de la cellule
     * @return la cellule
     */
    public Cell getCell(int width, int height) {
        return maze[height][width];
    }

    public Cell getCell(Coordinate coord) {
        return maze[coord.getRow()][coord.getCol()];
    }

    /**
     * retourne la largeur du labyrinthe
     * @return la largeur du labyrinthe
     */
    public int getWidth() {
        return width;
    }
    /**
     * retourne la hauteur du labyrinthe
     * @return la hauteur du labyrinthe
     */
    public int getHeight() {
        return height;
    }

    /**
     * retourne la position de l'entrée du labyrinthe
     * @return
     */
    public Coordinate getEntry() {
        return entry;
    }

    /**
     * retourne la position de la sortie du labyrinthe
     * @return
     */
    public Coordinate getExit() {
        return exit;
    }

    /**
     * modifie la position de l'entrée du labyrinthe
     * @param entry
     */
    public void setEntry(Coordinate entry) {
        this.entry = entry;
    }

    /**
     * modifie la position de la sortie du labyrinthe
     * @param exit
     */
    public void setExit(Coordinate exit) {
        this.exit = exit;
    }

    /**
     * retourne le labyrinthe
     * @return le labyrinthe
     */
    public  Cell[][] getMaze(){
        return this.maze;
    }
    /**
     * retoure le pourcentage de murs après génération du labyrinthe
     * @return
     */
    public Double getObstaclePercentage() {
        return obstaclePercentage;
    }

    /**
     * modifie le pourcentage de murs après génération du labyrinthe
     * @param obstaclePercentage
     */
    public void setObstaclePercentage(Double obstaclePercentage) {
        this.obstaclePercentage = obstaclePercentage;
    }

    /**
     * retourne les coordonnées d'un mur aléatoire présent dans la liste des murs
     * @return 
     */
    public Coordinate getRandomWall() {
        if (listWall.size() == 0) return null;
        int index = random.nextInt(listWall.size());
        return listWall.get(index);
    }
} 
