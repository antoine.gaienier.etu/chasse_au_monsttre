package main.Modele.map;

/**
 * Enumération des différents types de Boost disponibles
 */
public enum Boost {
    KILLWALL, FLASH; 


    //KILLWALL : supprime un mur aléatoire
    //FLASH : permet au chasser de découvrir la map dans un rayon de 3 cases
    //MULTISHOOT : permet au chasseur de tirer 2 fois au lieu d'une
}
