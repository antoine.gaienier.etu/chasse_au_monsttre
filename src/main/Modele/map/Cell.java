package main.Modele.map;

import fr.univlille.iutinfo.cam.player.perception.ICellEvent;

/**
 * Classe qui représente une cellule du labyrinth
 */
public class Cell implements ICellEvent {

    private CellInfo info;
    private int monsterTurn;
    private Coordinate coordinate;
    private Boost boost;

    /**
     * Constructor de la clase Cell
     * @param coordinate
     */
    public Cell(Coordinate coordinate) {
        this.coordinate = coordinate;
        this.info = CellInfo.EMPTY;
        this.monsterTurn = -1;
        this.boost = null;
    }

    /**
     * Retourne le type de la cellule
     * @return CellInfo
     */
    public CellInfo getState() {
        return info;
    }
    
    /**
     * Retourne la coordonnée de la cellule
     * @return Coordinate
     */
    public Coordinate getCoord() {
        return coordinate;
    }

    /**
     * Retourne le tour du monstre
     * @return
     */
    public int getTurn() {
        return monsterTurn;
    }







/// Ce qui est en dessous doit être modifié car inutilisable

    /**
     * Modifie le tour du monstre
     * @param monsterTurn
     */
    public void setMonsterTurn(int monsterTurn) {
        this.monsterTurn = monsterTurn;
    }

    /**
     * Modifie le type de la cellule
     * @param info
     */
    public void setInfo(CellInfo info){
        this.info = info;
    }

    /**
     * Modifie le boost de la cellule
     * @param boost
     */
    public void setBoost(Boost boost) {
        this.boost = boost;
    }

    /**
     * Retourne le boost de la cellule
     * @return Boost
     */
    public Boost getBoost() {
        return boost;
    }

    /**
     * Retourne toutes les informations de la cellule sous forme de String
     * @return String
     */
    public String toString(){
        return "Cell : " + coordinate.toString() + " " + info.toString() + " " + monsterTurn + " " + boost.toString() + "\n";
    }

}
