package main.Modele.player.playerRework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import fr.univlille.iutinfo.cam.player.hunter.IHunterStrategy;
import fr.univlille.iutinfo.cam.player.perception.ICellEvent;
import fr.univlille.iutinfo.cam.player.perception.ICoordinate;
import fr.univlille.iutinfo.cam.player.perception.ICellEvent.CellInfo;
import main.Modele.map.Cell;
import main.Modele.map.Coordinate;

public class HunterStrategyIA extends Strategy implements IHunterStrategy {
    protected ICellEvent[][] maze;
    protected ICellEvent bestCell = new Cell(new Coordinate(0, 0));
    protected int height;
    protected int width;
    protected int turn = 0;

    /**Initialise l'IA pour un hunter grâce aux dimensions du labyrinthe
     * @param arg0 la dimension x
     * @param arg1 la dimension y
     */
    @Override
    public void initialize(int arg0, int arg1) {
        this.maze = new ICellEvent[arg0][arg1];
        this.width = arg0;
        this.height = arg1;
    }

    /**Ajoute la cellule du labyrinthe spécifiée à celui que l'IA connait et vérifie si cette cellule est la meilleure information que l'IA aie
     * @param arg0 la cellule
     */
    @Override
    public void update(ICellEvent arg0) {
        setCell(arg0);
        updateBestCell(arg0);
    }

    /**Permet à l'IA de choisir une cellule pour jouer dépendemment des informations qu'elle détient
     * @return la cellule sélectionnée
     */
    @Override
    public ICoordinate play() {
        ICoordinate res;
        int range = 0;
        if (this.bestCell.getTurn() > 0) range = turn - bestCell.getTurn();
        if (range == 0) res = randomPick();
        else res = randomPick(coordInRange(bestCell.getCoord(), range));
        return res;
    }

    /**Crée la liste de Coordinate dans lesquelles un monstre d'une telle Coordinate peut être après un nombre de mouvements
     * Sont prits en compte le nombre de tours qu'il a pu traverser et la parité du tour pour plus de précision
     * @param coord la Coordinate du monstre
     * @param range le nombre de mouvements effectués depuis les coordonnées
     * @return la liste des coordonnées possibles à sa nouvelle position
     */
    private ArrayList<ICoordinate> coordInRange(ICoordinate coord, int range) {
        ArrayList<ICoordinate> res = new ArrayList<>();
        ICoordinate tmp;
        for (int i = -range; i <= range; i++) {
            for (int j = -range; j <= range; j++) {
                tmp = new Coordinate(coord.getRow()+i, coord.getCol()+j);
                if (Math.abs(i) + Math.abs(j) <= range && validCoord(tmp) 
                    && getCell(tmp).getState() != CellInfo.WALL
                    && (Math.abs(i) + Math.abs(j))%2 == range%2) {
                    res.add(tmp);
                }
            }
        }
        return res;
    }

    /**Vérifie qu'une Coordinate soit dans les limites du labyrinthe
     * @param coord la Coordinate à vérifier
     * @return true si valide
     */
    private boolean validCoord(ICoordinate coord) {
        return coord.getRow() > 0 && coord.getCol() > 0 &&  coord.getCol() < width &&  coord.getRow() < height;
    }

    /**Sélectionne une Coordinate aléatoirement à partir d'une ArrayList de Coordinate
     * @param list l'ArrayList dans laquelle choisir une Coordinate
     * @return la Coordinate aléatoirement sélectionnée
     */
    private ICoordinate randomPick(ArrayList<ICoordinate> list) {
        Collections.shuffle(list);
        return list.get(0);
    }

    /**Sélectionne une Coordinate aléatoirement dans tout le labyrinthe
     * @return la Coordinate aléatoirement sélectionnée
     */
    private ICoordinate randomPick() {
        Random rand = new Random();
        return new Coordinate(rand.nextInt(this.height-2)+1, rand.nextInt(this.width-2)+1);
    }

    private ICellEvent getCell(ICoordinate coord) {
        return this.maze[coord.getCol()][coord.getRow()];
    }

    private void setCell(ICellEvent cell) {
        this.maze[cell.getCoord().getCol()][cell.getCoord().getRow()] = cell;
    }

    /**Vérifie laquelle des cellules est la meilleure information entre l'ancienne meilleure et une nouvelle grâce au numéro de tour du monstre de celles-ci
     * @param res la nouvelle cellule
     */
    private void updateBestCell(ICellEvent res) {
        if (res.getTurn() > this.bestCell.getTurn()) {
            this.bestCell = res;
        }
    }
}
