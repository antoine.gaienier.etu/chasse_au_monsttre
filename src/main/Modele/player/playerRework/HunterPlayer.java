package main.Modele.player.playerRework;

import fr.univlille.iutinfo.cam.player.perception.ICellEvent.CellInfo;
import main.Modele.map.Boost;
import main.Modele.map.Cell;
import main.Modele.map.Coordinate;
import main.Modele.map.Mazes;
import main.java.fr.univlille.iutinfo.utils.Subject;

public class HunterPlayer extends Subject{
   
    private Mazes hunterMazes;
    private Mazes maze;
    private Cell bestMonsterTurn;
    private static int turn = 0;

    /**
     * Constructeur de la classe Hunter qiu prend en paramètre un labyrinthe
     * et initialise le labyrinthe du chasseur avec les mêmes dimensions
     * @param maze
     */
    public HunterPlayer(Mazes maze) {
        this.maze = maze;
        this.hunterMazes = new Mazes(maze.getWidth(), maze.getHeight(), (CellInfo) null);
        this.bestMonsterTurn = new Cell(new Coordinate(0, 0));
    }

    /**
     * retourne le labyrinthe du chasseur
     * @return
     */
    public Mazes getHunterMazes() {
        return hunterMazes;
    }

    /**
     * Permet au chasseur d'effectuer un tir sur une cellule/case du labyrinthe.
     * @param coord Coordonnées d'un tir du chasseur.
     * @return Retourne 'true' si le tir a été effectué, 'false' si 
     * le tir n'a pas été effectué.
     */
    public boolean shoot(Coordinate coord){
        turn++;    
        if (maze.getCell(coord).getState().equals(CellInfo.EXIT)) hunterMazes.getCell(coord).setInfo(CellInfo.WALL);
        else hunterMazes.getCell(coord).setInfo(maze.getCell(coord).getState());

        hunterMazes.getCell(coord).setMonsterTurn(maze.getCell(coord).getTurn());
        shootBoost(coord);

        updateBestMonsterTurn(coord);
        notifyObservers(coord);           
        return true;
    }

    /**
     * Permet de mettre à jour les cases du labyrinthe du chasseur
     * dans un rayon de 3 cases autour de la case tirée.
     * @param coord Coordonnées de la case tirée.
     */
    public void shootBoost(Coordinate coord){
        if(maze.getCell(coord).getBoost() == Boost.FLASH){
            //met a jour les casse dans un rayon de 3 cases
            for(int i = coord.getRow() - 2; i <= coord.getRow() + 2; i++){
                for(int j = coord.getCol() - 2; j <= coord.getCol() + 2; j++){
                    if(i >= 0 && i < maze.getWidth() && j >= 0 && j < maze.getHeight()){
                        addCase(new Coordinate(i, j));
                    }        
                }
            }
            maze.getCell(coord).setBoost(null);
        }       
    }

    /**
     * Permet de mettre à jour les cases du labyrinthe du chasseur pour le boost flash
     * @param c
     */
    private void addCase(Coordinate c) {
        switch (maze.getCell(c).getState()) {
            case EXIT:
                hunterMazes.getCell(c).setInfo(CellInfo.WALL);
                break;
            case MONSTER:
                hunterMazes.getCell(c).setMonsterTurn(turn+1);
                hunterMazes.getCell(c).setInfo(CellInfo.EMPTY);
                break;

            default:
                hunterMazes.getCell(c).setInfo(maze.getCell(c).getState());
                break;
        }

        hunterMazes.getCell(c).setMonsterTurn(maze.getCell(c).getTurn());
        updateBestMonsterTurn(c);
    }

    public Cell getBestCell() {
        return bestMonsterTurn;
    }

    /**
     * Retourne le tour 
     * @return
     */
    public static int getTurn() {
        return turn;
    }

    private void updateBestMonsterTurn(Coordinate coord) {
        if (this.hunterMazes.getCell(coord).getTurn() > this.bestMonsterTurn.getTurn()) {
            this.bestMonsterTurn = this.hunterMazes.getCell(coord);
        }
    }
}