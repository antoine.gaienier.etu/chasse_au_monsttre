package main.Modele.player.playerRework;

import main.Modele.map.Coordinate;
import main.Modele.map.Mazes;
import main.Modele.map.Voisin;
import main.Modele.player.Player;

public class MonsterPlayer extends Player{

    private boolean[][] maze;
    private Coordinate exit;

    /**
     * Constructeur de la classe Monster
     * @param coord
     * @param maze
     */
    public MonsterPlayer(Coordinate coord, Mazes maze) {
        super();
        this.coord = coord;
        initialize(maze);
    }

    /**
     * Constructeur de la classe Monster
     * @param coord
     * @param maze
     */
    public MonsterPlayer(Coordinate coord, boolean[][] maze) {
        super();
        this.coord = coord;
        this.maze =  maze;
    }

    /**
     * Initialise le labyrinthe de Mazes
     * @param maze
     */
    public void initialize(Mazes maze) {
        this.exit = maze.getExit();
    }

    /**
     * Modifie la coordonnée du Monster
     * @param x
     * @param y
     * @return
     */
    public boolean setCoord(int x, int y) {
        return setCoord(new Coordinate(x, y));
    }

    /**
     * Modifie la coordonnée du Monster
     * @param coord
     * @return
     */
    public boolean setCoord(Coordinate coord) {
        if (coord.getCol() >= maze.length || coord.getCol() < 0 || coord.getRow() >= maze[0].length || coord.getRow() < 0) return false;
        this.coord = coord;
        notifyObservers(coord);
        return true;
    }

    /**
     * Retourne la coordonnée du Monster
     * @return Coordinate
     */
    public Coordinate getCoord() {
        return this.coord;
    }

    /**
     * Retourne le labyrinthe de boolean
     * @return boolean[][]
     */
    public boolean[][] getMaze() {
        return maze;
    }

    /**
     * Retourne la sortie du labyrinthe
     * @return Coordinate
     */
    public Coordinate getExit() {
        return exit;
    }

    /**Vérifie que la coordonnée en paramètre est valide pour un déplacement, c'est-à-dire que la case n'est pas un mur 
     * et que la cordonnée est à la distance définie par les vecteurs de l'enum Voisin en partant des coordonnées du Monster
     * Si elle est correspondante, alors le Monster effectue le mouvement
     * @param newCoord la coordonnée à tseter
     * @return un booléen indiquant true si le Monster a pu se déplacer
     */
    public boolean isValidMove(Coordinate newCoord) {
        if (newCoord.getCol() >= maze.length || newCoord.getCol() < 0 || newCoord.getRow() >= maze[0].length || newCoord.getRow() < 0) return false;
        if(maze[newCoord.getRow()][newCoord.getCol()]) {
            return false;
        }
        int x;
        int y;
        for (Voisin v : Voisin.values()) {
            x = coord.getCol()+v.getDeltaX();
            y = coord.getRow()+v.getDeltaY();
            if (newCoord.getCol() == x && newCoord.getRow() == y) return setCoord(newCoord);
        }
        return false;
    }
}
