package main.Modele.player.playerRework;

import fr.univlille.iutinfo.cam.player.monster.IMonsterStrategy;
import fr.univlille.iutinfo.cam.player.perception.ICellEvent;
import fr.univlille.iutinfo.cam.player.perception.ICoordinate;
import main.Modele.map.Coordinate;
import main.java.fr.univlille.iutinfo.utils.Subject;

public class Monster extends Subject{
    protected IMonsterStrategy strat;
    protected MonsterPlayer monsterPlayer;
    private Coordinate coord;

    public Monster(ICellEvent monster, ICellEvent exit, boolean[][] maze) {
        this.coord = (Coordinate) monster.getCoord();
        this.strat = new MonsterStrategyIA();
        strat.initialize(maze);
        strat.update(monster);
        strat.update(exit);
    }

    public Monster(Coordinate monster, boolean[][] maze){
        this.monsterPlayer = new MonsterPlayer( monster, maze);
        this.coord = monster;
    }

    public ICoordinate play() {
        ICoordinate newCoord = strat.play();
        notifyObservers(newCoord);
        return newCoord;
    }

    public void update(ICellEvent arg0) {
        strat.update(arg0);
    }

    public Coordinate getCoord() {
        return coord;
    }

    public MonsterPlayer getMonsterPlayer() {
        return monsterPlayer;
    }

    public void setCoord(Coordinate coord) {
        this.coord = coord;
    }

}
