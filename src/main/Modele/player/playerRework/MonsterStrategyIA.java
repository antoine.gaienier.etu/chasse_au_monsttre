package main.Modele.player.playerRework;

import java.util.Stack;

import fr.univlille.iutinfo.cam.player.monster.IMonsterStrategy;
import fr.univlille.iutinfo.cam.player.perception.ICellEvent;
import fr.univlille.iutinfo.cam.player.perception.ICoordinate;
import main.Modele.map.Coordinate;

public class MonsterStrategyIA extends Strategy implements IMonsterStrategy {
    protected boolean[][] maze;
    protected ICoordinate coord = null;
    protected ICoordinate exit = null;
    protected Boolean marques[][];
    protected Stack<ICoordinate> path = new Stack<>();
    protected int index;

    /**Initialise l'IA grâce à un tableau de boolean représentant ub labyrinthe
     * @param arg0 un tableau de boolean dont les murs sont à true
     */
    @Override
    public void initialize(boolean[][] arg0) {
        this.marques = new Boolean[arg0.length][arg0[0].length];
        this.maze = arg0;
    }
    
    /**Remet à son état d'origine le tableau de marques et le path que l'IA compte prendre
     * Le tableau de marques sert à l'algorithme de recherche de path dans play()
     */
    public void reinitialize() {
        for (int i = 0; i < marques.length; i++) {
            for (int j = 0; j < marques[0].length; j++) {
                marques[i][j] = false;
            }
        }
        path.clear();
        index = 1;
    }
    
    /**Permet à l'IA de choisir une Coordonnée pour son tour
     * Selon le cas, play() effectue 4 actions différentes :
     * - Si l'IA n'a pas encore de path, elle en cherche un
     * - Si l'hunter tire sur le path, elle cherche un nouveau path pour contourner ce dernier tir
     * - Si le path est établi et que le hunter ne vient pas tirer dessus, l'IA continue son path
     * - Si un nouveau path ne peut pas être établi, l'IA se déplace dans l'ordre défini par orderVoisin()
     * @return la Coordinate sélectionnée ou null si l'IA n'a pas de point de départ
     */
    public ICoordinate play() {
        if (coord == null) return null;
        ///On continue notre path s'il est toujours valide

        if (!shootInvalidPath() && !path.isEmpty()) {
            index++;
            setCoord(path.get(index));
            return path.get(index);
        }


        reinitialize();
        path.add(this.coord);
        poserMarque(path.peek());

        ///On essaie de trouver un path, s'il n'y en a pas on agit aléatoirement
        if (this.exit == null || !findPath()) {
            reinitialize();
            setCoord(nextNeighboor(this.coord));
            return this.coord;
        }
        
        setCoord(path.get(index));
        return path.get(index);
    }

    /**Vérifie si le dernier tir du Hunter est sur le path de l'IA
     * @return true si les Coordinate du tir est sur le path
     */
    private boolean shootInvalidPath() {
        for (int i = 0; i < path.size(); i++) {
            if (i > index && equalsCoord(path.get(i), updCell.getCoord())) return true;
        }
        return false;
    }

    /**Cherche un path de la position actuelle de l'IA à exit
     * @return true si il existe un path
     */
    private boolean findPath() {
        ICoordinate currentCoord;
        while (!path.isEmpty()) {
            currentCoord = path.peek();
            if (currentCoord.getCol() == this.exit.getCol() && currentCoord.getRow() == this.exit.getRow()) return true;
            else checkDeadEnd(currentCoord);
        }
        return false;
    }

    /**Vérifie si la Coordinate spécifiée n'a plus de Coordinate suivante empruntable
     * Si elle n'en a plus, la retire du potentiel path choisi
     * Sinon, ajoute cette Coordinate au path de l'IA
     * @param coord la Coordinate à vérifie
     */
    private void checkDeadEnd(ICoordinate coord) {
        ICoordinate neighboor = nextNeighboor(coord);
        if (neighboor == null) path.pop();
        else {
            poserMarque(neighboor);
            path.push(neighboor);
        }
    }

    private void poserMarque(ICoordinate ICoordinate) {
        marques[ICoordinate.getRow()][ICoordinate.getCol()] = true;
    }

    /**Vérifie le prochain Voisin de la Coordinate spécifiée qui est valide selon l'ordre de orderVoisin()
     * @param coord2 la Coordinate spécifiée
     * @return le prochain Voisin valide ou null s'il n'y en a pas
     */
    private ICoordinate nextNeighboor(ICoordinate coord2) {
        for (Voisin v : orderVoisin()) {
            int x = coord2.getRow()+v.getDeltaX();
            int y = coord2.getCol()+v.getDeltaY();
            if (x >= 0 && y >= 0 && x < this.maze.length && y < this.maze[0].length 
                && (updCell == null || !equalsCoord(coord2, updCell.getCoord())) 
                && !this.maze[x][y] && !marques[x][y]) { 
                return new Coordinate(x, y);
            }
        }
        return null;
    }

    /**Change le positionnement de l'IA par la Coordinate spécifiée
     * @param ICoordinate le nouveau positionnement
     * @return true si la Coordinate est bien dans les dimensions du labyrinthe
     */
    public boolean setCoord(ICoordinate ICoordinate) {
        if (ICoordinate.getCol() >= maze.length || ICoordinate.getCol() < 0 || ICoordinate.getRow() >= maze[0].length || ICoordinate.getRow() < 0) return false;
        this.coord = ICoordinate;
        return true;
    }

    private boolean equalsCoord(ICoordinate coord,  ICoordinate other) {
        return coord.getCol() == other.getCol() && coord.getRow() == other.getRow();
    }

    /**Ordonne l'ordre des Voisin d'une cellule selon la position de l'IA par rapport à exit
     * Par exemple, si exit est en 5;5 et l'IA en 1;1, alors l'ordre sera DROITE, BAS, GAUCHE, HAUT
     * @return le tableau de Voisin ordonné
     */
    private Voisin[] orderVoisin() {
        Voisin v1;
        Voisin v2;
        if (coord.getCol() > exit.getCol()) v1 = Voisin.GAUCHE;
        else v1 = Voisin.DROITE;
        if(coord.getRow() > exit.getRow()) v2 = Voisin.HAUT;
        else v2 = Voisin.BAS;
        return Voisin.values(v1, v2);
    }

    /**Permet de donner à l'IA les informations dont elle a besoin sous forme de cellule
     * Si la cellule a l'état MONSTER, alors l'IA s'y positionne
     * Si la cellule a l'état EXIT, alors l'IA associe les Coordinate de cette cellule à exit
     * Si la cellule a l'état HUNTER, alors l'IA associe les Coordinate de cette cellule au dernier tir du chasseur
     * @param arg0
     */
    @Override 
    public void update(ICellEvent arg0) {
        switch (arg0.getState()) {
            case MONSTER:
                this.coord = arg0.getCoord();
                break;

            case EXIT:
                this.exit = arg0.getCoord();
                break;

            default:
                super.update(arg0);
                break;
        }
    }

    /**Enumération des différents voisins en classe interne afin que l'IA soit indépendante au possible
     */
    public enum Voisin {
        DROITE(0, 1),
        BAS(1, 0),
        GAUCHE(0, -1),
        HAUT(-1, 0);

        private int deltaX, deltaY;

        /**Retourne le déplacement en X
         * @return la taille du déplacement sur l'axe x
         */
        public int getDeltaX() {
            return deltaX;
        }

        /**Retourne le déplacement en Y
         * @return la taille du déplacement sur l'axe y
         */
        public int getDeltaY() {
            return deltaY;
        }

        /**Constructeur de la classe Voisin
         * @param x la taille du déplacement sur l'axe x
         * @param y la taille du déplacement sur l'axe y
         */
        private Voisin(int x, int y) {
            deltaX = x;
            deltaY = y;
        }

        /**Réordonne les voisins d'une case en mettant en premier les Voisin spécifiés
         * @param v1 le premier Voisin
         * @param v2 le deuxième Voisin
         * @return le tableau des voisins réordonnés
         */
        static public Voisin[] values(Voisin v1, Voisin v2) {
            Voisin[] res = new Voisin[4];
            for (Voisin voisin : values()) {
                if (voisin.equals(v1)) res[0] = voisin;
                else if (voisin.equals(v2)) res[1] = voisin;
                else if (res[2] == null) res[2] = voisin;
                else res[3] = voisin;
            }
            return res;
        }
    }
}
