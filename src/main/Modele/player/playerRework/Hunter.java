package main.Modele.player.playerRework;

import fr.univlille.iutinfo.cam.player.hunter.IHunterStrategy;
import fr.univlille.iutinfo.cam.player.perception.ICellEvent;
import fr.univlille.iutinfo.cam.player.perception.ICoordinate;
import main.Modele.map.Mazes;
import main.java.fr.univlille.iutinfo.utils.Subject;

public class Hunter extends Subject{
    protected IHunterStrategy strat;
    protected HunterPlayer hunterPlayer;


    public Hunter(int row, int col) {
        this.strat = new HunterStrategyIA();
        strat.initialize(row, col);
        hunterPlayer = null;
    }

    public Hunter(Mazes maze) {
        this.hunterPlayer = new HunterPlayer(maze);
        strat = null;
    }

    public ICoordinate play() {
        ICoordinate coord = strat.play();
        notifyObservers(coord);
        return coord;
    }


    public void update(ICellEvent arg0) {
        strat.update(arg0);
    }

    public HunterPlayer getHunterPlayer() {
        return hunterPlayer;
    }
}