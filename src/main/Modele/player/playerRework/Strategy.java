package main.Modele.player.playerRework;

import fr.univlille.iutinfo.cam.player.IStrategy;
import fr.univlille.iutinfo.cam.player.perception.ICellEvent;
import fr.univlille.iutinfo.cam.player.perception.ICoordinate;
import main.Modele.map.Coordinate;

public class Strategy implements IStrategy {
    protected ICellEvent updCell;

    @Override
    public ICoordinate play() {
        return new Coordinate(0, 0);
    }

    @Override
    public void update(ICellEvent arg0) {
        this.updCell = arg0;
    }
    
}
