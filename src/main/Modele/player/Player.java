package main.Modele.player;

import java.util.ArrayList;
import java.util.List;

import main.Strategy;
import main.Modele.map.Coordinate;
import main.java.fr.univlille.iutinfo.utils.Subject;

/**
 * Classe abstraite qui représente un joueur du jeu
 * le joueur peut être un monstre ou un chasseur
 */
public abstract class Player extends Subject{
    protected List<Strategy> attached;
    protected Coordinate coord;

    /**
     * Constructeur de la classe Player
     */
    protected Player() {
        attached = new ArrayList<>();
    }

    // /**
    //  * attache un observateur
    //  * @param stg
    //  */
    // public void attach(Strategy stg) {
    //     if (! attached.contains(stg)) {
    //         attached.add(stg);
    //     }
    // }

    // /**
    //  * détache un observateur
    //  * @param stg
    //  */
    // public void detach(Strategy stg) {
    //     if (attached.contains(stg)) {
    //         attached.remove(stg);
    //     }
    // }

    // /**
    //  * met à jour les observateurs
    //  */
    // protected void notifyStg() {
    //     for (Strategy o : attached) {
    //         o.update(this.coord);
    //     }
    // }

}