package main.Modele;

import main.Modele.map.Cell;
import main.Modele.map.Coordinate;
import main.Modele.map.Mazes;
import main.Modele.player.playerRework.Hunter;
import main.Modele.player.playerRework.Monster;
import main.Vue.Show;
import main.Vue.hunterVue;
import main.Vue.monsterVue;
import main.Vue.TurnStrategy.IAcIAStrategy;
import main.Vue.TurnStrategy.JcIAStrategy;
import main.Vue.TurnStrategy.JcJStrategie;
import main.Vue.TurnStrategy.TurnStrategy;
import fr.univlille.iutinfo.cam.player.perception.ICellEvent;
import fr.univlille.iutinfo.cam.player.perception.ICellEvent.CellInfo;

// import main.player.Hunter;
// import main.player.Monster;
// import main.player.MonsterStrategy;

/*
 * 
 * CLASSE MODELE
 * 
 */
public class Game{

    public static final String MONSTER = "Monster";
    public static final String HUNTER = "Hunter";

    private Show show;
    private Parametrage controller;

    private Monster monster;
    private Hunter hunter;

    private Mazes maze;
    private Coordinate ShootedCase;

    private int nbTurn;
    private String turn;

    private boolean monsterBot;
    private boolean hunterBot;
    
    private int widhtMaze = 8;
    private int heightMaze = 8;

    private double wallProportion = 0.7;
    private double boostProportion = 0.2;

    private monsterVue monsterVue;
    private hunterVue hunterVue;

    private TurnStrategy turnStrategy;

    /** Cette fonction démarre le programme du jeu
     */
    public void start() throws Exception {
        show = new Show(this);
        
        controller = new Parametrage(show, this);
        controller.afficherMenu();
    }

    public void initializeMaze(){
        maze = new Mazes(widhtMaze, heightMaze, wallProportion,boostProportion);
    }
    
    /**
     * Permet d'initialiser une partie
     * @param mBot Vraie pour si le monstre est une IA, faux sinon.
     * @param hBot Vraie pour si le chasseur est une IA, faux sinon.
     */
    public void initializeGame(boolean mBot, boolean hBot){
        initializeMaze();

        monsterBot = mBot;
        hunterBot = hBot;

        if(mBot){
            // monster = new MonsterStrategy(maze.MonsterPose(), maze);
            monster = new Monster((ICellEvent) maze.getCell(maze.getEntry()), (ICellEvent) maze.getCell(maze.getExit()), toMonsterTab(maze));
        }else{
            monster = new Monster(maze.getEntry(), toMonsterTab(maze));
        }

        if(hBot){
            hunter = new Hunter(maze.getHeight(), maze.getWidth());
        }else{
            hunter = new Hunter(maze);
        }

        initializeTurnStrategy(mBot, hBot);

        startGame();
    }

    private void initializeTurnStrategy(boolean mBot, boolean hBot) {
        if(mBot && hBot){
            turnStrategy = new IAcIAStrategy(this);
        }else if(!mBot && !hBot){
            turnStrategy = new JcJStrategie(this);
        }else{
            turnStrategy = new JcIAStrategy(this, mBot);
        }
    }

    /** Cette fonction initialise le Jeu
     */
    public void startGame(){
        nbTurn = 1;
        ShootedCase = null;
        turn = MONSTER;

        monsterVue = new monsterVue(maze, show, this);
        hunterVue = new hunterVue(new Mazes(maze.getWidth() , maze.getHeight(), null), show, this);

        monster.attach(monsterVue);
        hunter.attach(hunterVue);
        if(hunter.getHunterPlayer() != null) hunter.getHunterPlayer().attach(hunterVue);
        if(monster.getMonsterPlayer() != null) monster.getMonsterPlayer().attach(monsterVue);
        turnStrategy.jouerTour(); 
    }

    
    /** Permet de modifier le labyrinthe en fonction du déplacement du monstre
     * @param lastMonsterCoordinate Les coordonées du monstre avant son déplacement
     */
    public Cell modifyLabyrinthe(Coordinate newMonsterCoordinate){
        maze.getCell(monster.getCoord()).setInfo(CellInfo.EMPTY);
        maze.getCell(monster.getCoord()).setMonsterTurn(nbTurn);
        maze.getCell(newMonsterCoordinate).setInfo(CellInfo.MONSTER);
        monster.setCoord(newMonsterCoordinate);
        return maze.getCell(newMonsterCoordinate);
    }
    
    /** Permet de savoir si un joueur à gagner
     * @return Un String correspondant au gagnant, "Monster" pour le monstre, "Hunter" pour le chasseur.
     */
    public String win(){
        Coordinate m = monster.getCoord();
        if (m.getCol() == maze.getCell(ShootedCase).getCoord().getCol() && m.getRow() == maze.getCell(ShootedCase).getCoord().getRow()){
            return HUNTER;
        }else if(m.getCol() == maze.getExit().getCol() && m.getRow() == maze.getExit().getRow()){
            return MONSTER;
        }
        return "";
    }

    /**
     * Permet de changer le joueur qui doit jouer, passer de Monstre à Chasseur, et Vice-versa
     */
    public void changeTurn(){
        if(turn.equals(HUNTER)){
            setTurn(MONSTER);
        }else{
            setTurn(HUNTER);
        }
    }

    /**
     * Permet de jouer un tour de jeu.
     */
    public void playTurn(){
        boolean gagner = false;
        if(turn.equals(HUNTER)){
            incrementNbTurn();
            gagner = show.showWin();
        }
        if(!gagner){
            changeTurn();
            turnStrategy.jouerTour(); 
        }
    }

    /**Converti un tableau de Cell en tableau de boolean
     * @param maze le Mazes à convertir
     * @return un tableau de boolean à 2 dimenssions où true correspond à WALL et false correspond à EMPTY
     */
    public boolean[][] toMonsterTab(Mazes maze) {
        boolean[][] res = new boolean[maze.getHeight()][maze.getWidth()];
        for (int i = 0; i < maze.getWidth() ; i++) {
            for (int j = 0; j < maze.getHeight() ; j++) {
                if (maze.getCell(i, j).getState().equals(CellInfo.WALL)) {
                    res[j][i] = true;
                } else res[j][i] = false;
            }
        }
        return res;
    }
    
    /**
     * Retourne le monstre
     * @return Monster
     */
    public Monster getMonster() {
        return monster;
    }

    /**
     * Retourne le chasseur
     * @return
     */
    public Hunter getHunter() {
        return hunter;
    }

    /**
     * Retourne le show
     * @return
     */
    public Show getShow() {
        return show;
    }

    /**
     * Permet de savoir si le monstre est une IA
     * @return
     */
    public boolean isMonsterBot() {
        return monsterBot;
    }

    /**
     * Permet de savoir si le chasseur est une IA
     * @return
     */
    public boolean isHunterBot() {
        return hunterBot;
    }

    /**
     * Retourne le labyrinthe
     * @return
     */
    public Mazes getMaze() {
        return maze;
    }

    /**
     * Retourne la case tirée par le chasseur
     * @return
     */
    public Coordinate getShootedCase() {
        return ShootedCase;
    }

    /**
     * Retourne le nombre de tour
     * @return
     */
    public int getNbTurn() {
        return nbTurn;
    }

    /**
     * Retourne le tour sous forme de String
     * @return
     */
    public String getTurn() {
        return turn;
    }

    /**
     * Modifie la case tirée par le chasseur
     * @param shootedCase
     */
    public void setShootedCase(Coordinate shootedCase) {
        ShootedCase = shootedCase;
    }

    /**
     * incrémente le nombre de tour
     */
    public void incrementNbTurn() {
        this.nbTurn ++;
    }

    /**
     * Modifie le tour
     * @param turn
     */
    public void setTurn(String turn) {
        this.turn = turn;
    }
    
    /**
     * Modifie la largeur du labyrinthe
     * @param widhtMaze
     */
    public void setWidhtMaze(int widhtMaze) {
        this.widhtMaze = widhtMaze;
    }

    /**
     * Modifie la hauteur du labyrinthe
     * @param heightMaze
     */
    public void setHeightMaze(int heightMaze) {
        this.heightMaze = heightMaze;
    }

    /**
     * Modifie la proportion de mur pour la génération du labyrinthe
     * @param wallProportion
     */
    public void setWallProportion(double wallProportion) {
        this.wallProportion = wallProportion;
    }

    /**
     * Modifie la proportion de boost pour la génération du labyrinthe
     * @param boostProportion
     */
    public void setBoostProportion(double boostProportion) {
        this.boostProportion = boostProportion;
    }

    /**
     * Renvoie la valeur de la largeur du labyrinthe
     * @return La largeur du labyrinthe
     */
    public int getWidhtMaze() {
        return widhtMaze;
    }

    /**
     * Renvoie la valeur de la hauteur du labyrinthe
     * @return La hauteur du labyrinthe
     */
    public int getHeightMaze() {
        return heightMaze;
    }

    /**
     * Renvoie la vue du monstre
     * @return La vue du monstre
     */
    public monsterVue getMonsterVue() {
        return monsterVue;
    }

    /**
     * Renvoie la vue du chasseur
     * @return La vue du chasseur
     */
    public hunterVue getHunterVue() {
        return hunterVue;
    }
}