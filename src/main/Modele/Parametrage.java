package main.Modele;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextField;
import main.Launch;
import main.Vue.Show;

public class Parametrage {

    private Show show;
    private Game game;

    public Parametrage(Show show, Game game) {
        this.show = show;
        this.game = game;
    }

    public Parametrage() {
        this.game = Launch.getGame();
        this.show = game.getShow();
    }

    ////////////////// Afficher ecran /////////////////////////////

    /**
     * Permet de charger et d'afficher le fichier FXML de choix des Joueurs
     * @throws IOException
     */
    public void afficherJouer() throws IOException{
        show.Charge("/ressource/fxml/ChoixJoueur.fxml", "Jouer");
    }

    /**
     * Permet de charger et d'afficher le fichier FXML des parametre
     * @throws IOException
     */
    public void afficherParametre() throws IOException{
        show.Charge("/ressource/fxml/Parametre.fxml", "Paramétre");
    }

    /**
     * Permet de charger et d'afficher le fichier FXML du Menu
     * @throws IOException
     */
    public void afficherMenu() throws IOException{
        show.Charge("/ressource/fxml/Menu.fxml", "Menu");
    }


    ////////////////// Parametre /////////////////////////////

    @FXML
    private TextField widhtMaze;

    // @FXML
    // private TextField heightMaze;

    @FXML
    private TextField wallProportion;

    @FXML
    private TextField boostProportion;

    @FXML
    private ColorPicker wallColor;

    @FXML
    private ColorPicker emptyColor;

    @FXML
    private ColorPicker monsterColor;

    @FXML
    private ColorPicker hunterColor;

    @FXML
    private ColorPicker exitColor;

    /**
     * Permet de sauvegarder les changements fait dans les Paramétres
     */
    public void sauvegarderParam(){
        game.setWidhtMaze(Integer.parseInt(widhtMaze.getText()));
        game.setHeightMaze(Integer.parseInt(widhtMaze.getText()));

        if(isPercentage(Integer.parseInt(wallProportion.getText()))){
            game.setWallProportion(Double.parseDouble(wallProportion.getText())/100);
        }

        if(isPercentage(Integer.parseInt(boostProportion.getText()))){
            game.setBoostProportion(Double.parseDouble(boostProportion.getText())/100);
        }
        CaseInfo.WALL.setColor(wallColor.getValue());
        CaseInfo.EMPTY.setColor(emptyColor.getValue());
        CaseInfo.MONSTER.setColor(monsterColor.getValue());
        CaseInfo.HUNTER.setColor(hunterColor.getValue());
        CaseInfo.EXIT.setColor(exitColor.getValue());
        try {
            afficherMenu();
        }catch (IOException e){e.printStackTrace();}
    }

    /**
     * Permet de verifier si la valeur donnée est bien un pourcentage compris entre 0 et 100.
     * @param i la valeur donnée par l'utilisateur
     * @return Vrai si la valeur est valide, faux sinon.
     */
    public boolean isPercentage(int i){
        return i <= 100 && i >= 0;
    }


    ////////////////// ChoixJoueur /////////////////////////////

    /**
     * Permet d'initialiser une partie en Joueur conte Joueur
     */
    public void playJcJ(){
        game.initializeGame(false, false);
    }

    /**
     * Permet de charger et d'afficher le fichier FXML du choix de votre personnage dans Joueur contre IA
     * @throws IOException
     */
    public void playJcIA() throws IOException{
        show.Charge("/ressource/fxml/ChoixCamp.fxml", "Choix Personnage");
    }

    /**
     * Permet d'initialiser une partie en IA contre IA
     */
    public void playIAcIA(){
        game.initializeGame(true, true);
    }

    /////////////// ChoixCamp ////////////////////////
    
    /**
     * Permet d'initialiser une partie en Joueur contre IA en jouant le Monstre
     */
    public void playMonstercIA(){
        game.initializeGame(false, true);
    }

    /**
     * Permet d'initialiser une partie en Joueur contre IA en jouant le Monstre
     */
    public void playHuntercIA(){
        game.initializeGame(true, false);
    }

    /////////////// Victoire ////////////////////////

    /**
     * Permet de relancer la partie dans la même configuration que la partie précédente
     */
    public void restart(){
        game.initializeGame(game.isMonsterBot(), game.isHunterBot());
    }

}
