package main.Vue;

import fr.univlille.iutinfo.cam.player.perception.ICellEvent.CellInfo;
import javafx.scene.layout.VBox;
import main.Modele.Game;
import main.Modele.map.Cell;
import main.Modele.map.Coordinate;
import main.Modele.map.Mazes;
import main.java.fr.univlille.iutinfo.utils.Observer;
import main.java.fr.univlille.iutinfo.utils.Subject;

public class hunterVue extends PlayerVue implements Observer{

    public hunterVue(Mazes maze, Show show, Game game) {
        super(show, game);
        VBox vbox = initialiseMaze(maze);
        this.scene = createScene(vbox);
    }

    /**
     * Permet d'afficher la Scéne du chasseur
     */
    public void afficheScene(){
        setLabel();
        show.showStage(scene, "Hunter");
    }

    @Override
    public void update(Subject subj) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'update'");
    }

    @Override
    public void update(Subject subj, Object data) {
        //recupere la coordone en donnée
        Coordinate coord = (Coordinate) data;

        // recupere la cellule sur laquelle le chasseur vient de tirer
        Cell cell = game.getMaze().getCell(coord);

        if(cell.getState().equals(CellInfo.EXIT)){
            cell.setInfo(CellInfo.WALL);
        }

        // Crée une nouvelle case avec les info de la cellule
        Case cases = new Case(cell, show);

        // Modifie les case dans la vue
        setGraphicMazeCell(coord, cases.showCase(false));

        game.setShootedCase(coord);
        if(game.getMonster().getMonsterPlayer() == null) game.getMonster().update(cell);

        game.playTurn();
    }

}
