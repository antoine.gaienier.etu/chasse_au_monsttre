package main.Vue;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import main.Modele.CaseInfo;
import main.Modele.Game;
import main.Modele.map.Cell;

/*
 * 
 * CLASSE QUI RENVOIT UN RECTANGLE SERVANTS à AFFICHER UNE CASE
 * 
 */

public class Case{

    private Cell cell;
    private Show show;

    /** Constructeur de la clase Case
     * @param cell une cell d'un labyrinthe
     * @param gm un GraphicMazes auquel la case est rattacher
     */
    public Case(Cell cell, Show show){
        this.cell = cell;
        this.show = show;
    }

    /**Crée un Node contenant un rectangle de couleur
     * @param color La Couleur du rectangle
     * @return Un Node du rectangle
     */
    public static Node showRectangleCase(Color color){
        Rectangle square = createSquare();
        square.setFill(color);
        return square;
    }

    /**Crée un Node contenant un StackPane avec un rectangle de couleur et un texte au millieu
     * @param color La Couleur du rectangle
     * @param titre Le texte au centre du rectangle
     * @return Un Node du StackPane
     */
    public static Node showStackPaneNode(Color color, String titre){
        Rectangle square = createSquare();
        square.setFill(color);
        
        Label l = new Label(titre);

        StackPane stack = new StackPane();
        stack.getChildren().addAll(square, l);
        return stack;
    }

    /** Permet de crée un Rectangle, base de toutes les cases
     * @return Un Rectangle, correspondant a une case avec sa taille et sa position en paramètre
     */
    public static Rectangle createSquare(){
        Rectangle square = new Rectangle();
        square.setX(0);
        square.setY(0);
        square.setWidth(25);
        square.setHeight(25);

        return square;
    }

    /**ajoute un rectangle rouge transparant au node afin d'afficher une case rouge désignant dernière case viser par le chasseur
     * @param node Le node de la case a colorié
     * @return Un Node du StackPane
     */
    public static Node showShootedCase(Node node){
        Rectangle square = createSquare();
        square.setFill(CaseInfo.HUNTER.getColor());
        square.setOpacity(0.3);

        StackPane stack = new StackPane();
        stack.getChildren().addAll(node, square);

        return stack;
    }

    /** Permet de retourner une case cliquable à partir des info de la cellule
     * @return Un Node, correspondant a la case pour l'utiliser dans des HBox
     */
    public Node showCase(boolean shooted){
        Node n = null;

        if(cell == null || cell.getState() == null){
            n = showRectangleCase(Color.BLACK);
        }else{
            for(CaseInfo c : CaseInfo.values()){
                if(c.getCellInfo().equals(cell.getState())){
                    if(c.equals(CaseInfo.EMPTY) && cell.getTurn() > 0){
                        n = showStackPaneNode(CaseInfo.EMPTY.getColor(), "" + cell.getTurn());
                    }else{
                       n = showStackPaneNode(c.getColor(), c.getTitle()); 
                    }
                }
            } 
        }

        if(shooted){
            n = showShootedCase(n);
        }
        
        n.setOnMouseClicked(e -> {
            clickCaseAction();
        });
        return n;
    }

    /** Fait joue le Monstre ou je Chasseur lors d'un clique sur une case
     * @param e un MouseEvent d'un clique sur la case
     */
    public void clickCaseAction(){
        if(show.getGame().getTurn().equals(Game.MONSTER)){
            if(show.getGame().isMonsterBot()){
                show.getGame().getMonster().play();
            }else{
                show.getGame().getMonster().getMonsterPlayer().isValidMove(this.cell.getCoord());
            }
        }else{
            if(show.getGame().isHunterBot()){
                show.getGame().getHunter().play();
            }else{
                show.getGame().getHunter().getHunterPlayer().shoot(cell.getCoord());
            }
        }
    }
}
