package main.Vue;

import javafx.scene.layout.VBox;
import main.Modele.Game;
import main.Modele.map.Cell;
import main.Modele.map.Coordinate;
import main.Modele.map.Mazes;
import main.java.fr.univlille.iutinfo.utils.Observer;
import main.java.fr.univlille.iutinfo.utils.Subject;

public class monsterVue extends PlayerVue implements Observer{

    public monsterVue(Mazes maze, Show show, Game game) {
        super(show, game);
        VBox vbox = initialiseMaze(maze);
        this.scene = createScene(vbox);
    }

    /**
     * Permet d'afficher la Scéne du monstre
     */
    public void afficheScene(){
        setLabel();
        
        if(game.getShootedCase() != null){
            Cell cellShootedCase = game.getMaze().getCell(game.getShootedCase());
            Case caseShootedCase = new Case(cellShootedCase, show);
            setGraphicMazeCell(game.getShootedCase(), caseShootedCase.showCase(true));
        }
        show.showStage(scene, "Monster");
    }

    @Override
    public void update(Subject subj) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'update'");
    }

    @Override
    public void update(Subject subj, Object data) {
        //recupere la coordone en donnée
        Coordinate coord = (Coordinate) data;

        // recupere la cellule ou se trouvait le monstre avant déplacement
        Cell cell = game.getMaze().getCell(game.getMonster().getCoord());
        // recupere la cellule ou se trouve le monstre
        Cell cellMonster = game.modifyLabyrinthe(coord);
        
        // Crée une nouvelle case avec les info de la premiere cellule
        Case cases = new Case(cell, show);
         // Crée une nouvelle case avec les info de la seconde cellule
        Case cases2 = new Case(cellMonster, show);

        // Modifie les case dans la vue
        setGraphicMazeCell(cell.getCoord(), cases.showCase(false));
        setGraphicMazeCell(cellMonster.getCoord(), cases2.showCase(false));

        // remplace l'ancienne case shootée par la case normale.
        if(game.getShootedCase() != null){
            Coordinate oldShootedCase = game.getShootedCase();
            game.setShootedCase(null);
            Cell cellShootedCase = game.getMaze().getCell(oldShootedCase);
            Case caseShootedCase = new Case(cellShootedCase, show);
            setGraphicMazeCell(oldShootedCase, caseShootedCase.showCase(false));
        }

        game.playTurn();
    }
}
