package main.Vue.TurnStrategy;

import main.Modele.Game;

public class JcIAStrategy implements TurnStrategy {

    private Game game;
    private String bot;

    /**
     * Permet d'initialiser la strategie Joueur contre IA.
     * @param game
     * @param isMonsterBot
     */
    public JcIAStrategy(Game game, boolean isMonsterBot) {
        this.game = game;

        if(isMonsterBot){
            this.bot = Game.MONSTER;
        }else {
            this.bot = Game.HUNTER;
        }
    }

    @Override
    public void jouerTour() {
        if(isBotTurn()){
            if(bot.equals(Game.MONSTER)){
                game.getMonster().play();
            }else{
                game.getHunter().play();
            }
        }else{ 
            game.getShow().afficherLabyrinthe();
        }
    }

    /**
     * Indique si c'est au tour de l'IA de jouer dans une partie Joueur contre IA.
     * @return vraie si c'est au tour de l'IA, faux sinon.
     */
    public boolean isBotTurn(){
        if(game.getTurn().equals(Game.HUNTER) && game.isHunterBot()){
            return true;
        }else if(game.getTurn().equals(Game.MONSTER) && game.isMonsterBot()){
            return true;
        }
        return false;
    }
}
