package main.Vue.TurnStrategy;

import main.Modele.Game;

public class IAcIAStrategy implements TurnStrategy {

    private Game game;

    /**
     * Permet d'initialiser la strategie IA contre IA.
     * @param game
     */
    public IAcIAStrategy(Game game) {
        this.game = game;
    }

    @Override
    public void jouerTour() {
        game.getShow().afficherLabyrinthe();
    }
    
}
