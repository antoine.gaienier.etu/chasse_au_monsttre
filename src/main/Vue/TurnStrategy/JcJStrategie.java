package main.Vue.TurnStrategy;

import main.Modele.Game;

public class JcJStrategie implements TurnStrategy {

    private Game game;

    /**
     * Permet d'initialiser la strategy Joueur contre Joueur
     * @param game
     */
    public JcJStrategie(Game game) {
        this.game = game;
    }

    @Override
    public void jouerTour() {
        game.getShow().nextTurn();
    }
    
}
