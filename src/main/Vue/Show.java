package main.Vue;

import java.io.IOException;
import java.net.URL;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import main.Modele.Game;
import main.Modele.map.Boost;
import main.Modele.map.Coordinate;

/*
 * 
 * CLASSE VUE
 * 
 */
public class Show{

    private static Stage stage;
    private Game game;

    public Show(Game game) {
        this.game = game;
        stage = new Stage();
    }

    /** Permet d'afficher une scene et un titre à l'écran dans une fenêtre
     * @param scene la scene à afficher
     * @param title le titre de la scène à afficher
     */
    public void showStage(Scene scene, String title){
        stage.setTitle(title);
        stage.setScene(scene);
        stage.show();
    }
    
    /** Permet d'afficher une scene fxml
     * @param fichier Un string correspondant au chemin du fichier
     * @param title le titre de la scène à afficher
     */
    public void Charge(String fichier, String title) throws IOException {
        URL fxmlFileUrl = getClass().getResource(fichier);
        if (fxmlFileUrl == null) {
            System.out.println("Impossible de charger le fichier fxml");
            System.exit(-1);
        }
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(fxmlFileUrl);
        Parent root = loader.load();
        if(stage.isMaximized()) {
            stage.setMaximized(true);
            stage.setScene(new Scene(root, 1920, 1080));
        }
        stage.setScene(new Scene(root));
        stage.setTitle(title);
        stage.show();
    }

    /** Permet le changement de joueur aprés que l'un est joué son tour
     */
    public void nextTurn(){
        VBox v = new VBox(3);
        v.setPadding(new Insets (3 , 3 , 3 , 3));
        v.setAlignment(Pos.CENTER);
        Label label = new Label("C'est au tour du " + game.getTurn() + ".\n Cliquez sur la fenêtre quand vous êtes prêt");
        v.getChildren().addAll(label);
        showStage(new Scene(v),"changement de joueur");

        v.setOnMouseClicked(e -> {
            afficherLabyrinthe();
        });
    }

    /** Permet de choisir quel vue afficher, monstre ou hunter
     */
    public void afficherLabyrinthe(){
        if(game.getTurn().equals(Game.MONSTER)){
            game.getMonsterVue().afficheScene();
        }else{
            game.getHunterVue().afficheScene();
        }
    }
    
    /**
     * Vérifie si quelqu'un a gagner et montre la prochaine scéne en conséquence.
     * @return Si quelqu'un a gagner ou non
     */
    public boolean showWin(){
        String win = game.win();
        if(win.equals(Game.MONSTER)){
            try {
                Charge("/ressource/fxml/VictoireMonstre.fxml", "Monster Win");
            } catch (IOException e){ e.printStackTrace();}
            return true;
        }else if(win.equals(Game.HUNTER)){
            try {
                Charge("/ressource/fxml/VictoireChasseur.fxml", "Hunter Win");
            } catch (IOException e){  e.printStackTrace();}
            return true;
        }
        return false;
    }

    /**
     * Vérifie si on a tirer sur une case Boots Killwall et Permet si c'est le cas d'enlever un mur aléatoirement sur le labyrinthe.
     * @param coord La coordonée de la Case.
     */
    public void boostWall(Coordinate coord){
        if(game.getMaze().getCell(coord).getBoost() == Boost.KILLWALL){
            //enleve un mur du labyrinthe de maniere aleatoire
            Coordinate c = game.getMaze().getRandomWall();
            game.getMaze().removeOneWall(c);
            game.getMaze().getCell(coord).setBoost(null);
        }
    }
    
    public Game getGame() {
        return game;
    }
}