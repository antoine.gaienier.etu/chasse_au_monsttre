package main.Vue;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import main.Modele.Game;
import main.Modele.map.Coordinate;
import main.Modele.map.Mazes;

public class PlayerVue{

    protected Show show;
    protected Scene scene;
    protected Game game;

    public PlayerVue(Show show, Game game) {
        this.show = show;
        this.game = game;
    }

    /** Permet d'afficher une scene et un titre à l'écran dans une fenêtre
     * @param scene la scene à afficher
     * @param title le titre de la scène à afficher
     */
    public void afficheScene(Scene scene, String title){
        show.showStage(scene, title);
    }

    /** Permet d'afficher la vue du Joueur qui joue, lavue du Monstre pour le Monstre, la vue du Chasseur pour le Chasseur
     * @return Une VBox avec une grille de case correspondant à la vue du joueur
     */
    public VBox initialiseMaze(Mazes maze){
        VBox v = new VBox();
        HBox h;
        for(int i = 0; i < maze.getHeight(); i++){
            h = new HBox();
            for(int j = 0 ;j <  maze.getWidth() ; j++){
                h.getChildren().add(new Case(maze.getCell(j,i), show).showCase(false));
                h.setSpacing(0.5);
            }
            v.getChildren().add(h);
            v.setSpacing(0.5);
        }
        return v;
    }

    /**
     * Crée une nouvelle Scene pour afficher le labyrinthe
     * @param vbox Une VBox contenant toutes les Cases du labyrinthe.
     * @return la scene crée
     */
    public Scene createScene(VBox vbox) {
        VBox v = new VBox();
        HBox h = new HBox();

        v.setSpacing(20);
        v.setAlignment(Pos.CENTER);
        h.setAlignment(Pos.CENTER);

        Label l = new Label("Tour de jeu :" + game.getNbTurn());

        VBox jeu = vbox;

        ScrollPane scrollPane = new ScrollPane(jeu);
        double taille = (game.getMaze().getWidth()+1.75)*25 -2.5 ;
        if(taille < 2500){
            scrollPane.setPrefSize(taille, taille);
        }else{
            scrollPane.setPrefSize(2000, 2000);
        }
        
        h.getChildren().add(scrollPane);
        v.getChildren().addAll(l,h);

        return new Scene(v);
    }

    /**
     * Change la Case aux Coordonnée donné avec une nouvelle Case.
     * @param coord les Coordonée de la case à changer
     * @param node La nouvelle Case
     */
    public void setGraphicMazeCell(Coordinate coord, Node node){
        int x = coord.getCol();
        int y = coord.getRow();
        ((HBox) getGraphicMaze().getChildren().get(y)).getChildren().set(x, node);
    }

    /**
     * Renvoie la VBox contenant tout le labyrinthe graphique 
     * @return Une VBox
     */
    public VBox getGraphicMaze(){
        return (VBox) getScrollPane().contentProperty().getValue();
    }

    /**
     * Renvoie Le scrollPane utilisé dans la scene.
     * @return le ScrollPane
     */
    public ScrollPane getScrollPane(){
        return (ScrollPane) ((HBox) getSceneChildren().getChildren().get(1)).getChildren().get(0); 
    }

    /**
     * renvoie l'enfant de la Scene
     * @return l'enfant de la scene
     */
    public VBox getSceneChildren(){
        return ((VBox) scene.getRoot());
    }

    /**
     * Change le Label qui indique le tour actuel du jeu.
     */
    public void setLabel(){
        Label l = new Label("Tour de jeu :" + game.getNbTurn());
        getSceneChildren().getChildren().set(0, l);
    }
}